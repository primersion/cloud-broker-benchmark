#ifndef MATRIX_UTIL
#define MATRIX_UTIL

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

double **allocateMat(int rows, int cols) {
  double **mem = (double **)malloc(sizeof(double *) * rows);
  int i;

  for (i = 0; i < rows; i++) {
    mem[i] = (double *)malloc(sizeof(double) * cols);
  }

  return mem;
}

void freeMat(double **mat, int rows) {
  int i;
  for (i = 0; i < rows; i++) {
    free(mat[i]);
  }
  free(mat);
}

void initMat(double **mat, int rows, int cols) {
  int i, j;
  for (i = 0; i < rows; i++)
    for (j = 0; j < cols; j++) {
      mat[i][j] = rand();
    }
}

void printMat(double **a, int rows, int cols) {
  int i, j;

  for (i = 0; i < rows; i++) {
    for (j = 0; j < cols; j++)
      printf("%f ", a[i][j]);
    printf("\n");
  }

  printf("\n");
}

#endif // MATRIX_UTIL
