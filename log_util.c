#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

long double cpu_usage();

int main(void) {
	
  while(1) {
		printf("%2.2Lf\n", cpu_usage() * 100);
		//printf("CPU usage: %2.2Lf%%   \r", cpu_usage() * 100);
		fflush(stdout);
  }
  return(0);
}

long double cpu_usage() {

	long double a[4], b[4];
	FILE *fp;
	
  fp = fopen("/proc/stat","r");
  fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
  fclose(fp);
	sleep(1);

  fp = fopen("/proc/stat","r");
  fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
  fclose(fp);

	return ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
}
