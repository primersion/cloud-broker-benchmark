#!/bin/bash

cmake .
make all

numProc=`grep -c ^processor /proc/cpuinfo`
bucketName=$1
region=$2
instanceName=$3
s3Path="s3://"$bucketName"/log/"$region"/"$instanceName"/"
size=2000
iterations=5

for (( i = 1; i <= iterations; i=i+1 )) do
    fileName="log_"$size"_"$numProc"_"$i".csv"
    dstat -T -c -m --noheaders --output $fileName &
    mpirun -n $numProc bin/matrix_mul_mpi $size
    kill $!
    aws s3 mv $fileName $s3Path$fileName --region eu-central-1
done
