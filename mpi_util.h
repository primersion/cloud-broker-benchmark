#ifndef MPI_UTIL
#define MPI_UTIL

#include <mpi.h>
#define MPI_MASTER 0

struct MPIState {
  int rank_id;
  int num_proc;
  int send_dest;
  int recv_source;
  int msg_tag;
  MPI_Datatype msg_type;
  MPI_Status status;
};

struct MPIState initMPI(int argc, char* argv[]) {
  MPI_Init(&argc, &argv);

  struct MPIState mpi_state;
  mpi_state.send_dest = MPI_MASTER;
  mpi_state.recv_source = MPI_MASTER;
  mpi_state.msg_tag = 0;
  mpi_state.msg_type = MPI_INT;

  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_state.rank_id);
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_state.num_proc);

  return mpi_state;
}

int MPISend(struct MPIState* state, void* data, int count) {
  return MPI_Send(data, count, state->msg_type, state->send_dest,
                  state->msg_tag, MPI_COMM_WORLD);
}

int MPIReceive(struct MPIState* state, void* data, int count) {
  return MPI_Recv(data, count, state->msg_type, state->recv_source,
                  state->msg_tag, MPI_COMM_WORLD, &state->status);
}

int MPIBroadcast(struct MPIState* state, void* data, int count, int root) {
  return MPI_Bcast(data, count, state->msg_type, root, MPI_COMM_WORLD);
}

int MPIScatter(struct MPIState* state, void* send_buf, void* recv_buf,
               int count, int root) {
  return MPI_Scatter(send_buf, count, state->msg_type, recv_buf, count,
                     state->msg_type, root, MPI_COMM_WORLD);
}

int MPIGather(struct MPIState* state, void* send_buf, void* recv_buf, int count,
              int root) {
  return MPI_Gather(send_buf, count, state->msg_type, recv_buf, count,
                    state->msg_type, root, MPI_COMM_WORLD);
}

#endif // MPI_UTIL
